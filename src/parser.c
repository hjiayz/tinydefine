#include <tree_sitter/parser.h>

#if defined(__GNUC__) || defined(__clang__)
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmissing-field-initializers"
#endif

#define LANGUAGE_VERSION 14
#define STATE_COUNT 49
#define LARGE_STATE_COUNT 2
#define SYMBOL_COUNT 37
#define ALIAS_COUNT 0
#define TOKEN_COUNT 18
#define EXTERNAL_TOKEN_COUNT 0
#define FIELD_COUNT 3
#define MAX_ALIAS_SEQUENCE_LENGTH 4
#define PRODUCTION_ID_COUNT 3

enum {
  sym_eol = 1,
  aux_sym_global_annotation_token1 = 2,
  anon_sym_EQ = 3,
  sym_ident = 4,
  aux_sym_annotation_token1 = 5,
  anon_sym_f32 = 6,
  anon_sym_f64 = 7,
  anon_sym_bytes = 8,
  sym_digit = 9,
  anon_sym_DOT_DOT = 10,
  anon_sym_PIPE = 11,
  anon_sym_SLASH = 12,
  anon_sym_SLASH2 = 13,
  sym_regex_pattern = 14,
  anon_sym_COLON = 15,
  anon_sym_LBRACK_RBRACK = 16,
  anon_sym_AMP = 17,
  sym_source_file = 18,
  sym_global_annotation_or_empty = 19,
  sym_global_annotation = 20,
  sym_statement = 21,
  sym_annotation = 22,
  sym_define = 23,
  sym_sample_type = 24,
  sym_integer_range = 25,
  sym_integer_expr = 26,
  sym_integer_type = 27,
  sym_string_type = 28,
  sym_composite_field = 29,
  sym_call_type = 30,
  sym_alias_type = 31,
  aux_sym_source_file_repeat1 = 32,
  aux_sym_source_file_repeat2 = 33,
  aux_sym_statement_repeat1 = 34,
  aux_sym_integer_expr_repeat1 = 35,
  aux_sym_integer_type_repeat1 = 36,
};

static const char * const ts_symbol_names[] = {
  [ts_builtin_sym_end] = "end",
  [sym_eol] = "eol",
  [aux_sym_global_annotation_token1] = "global_annotation_token1",
  [anon_sym_EQ] = "=",
  [sym_ident] = "ident",
  [aux_sym_annotation_token1] = "annotation_token1",
  [anon_sym_f32] = "f32",
  [anon_sym_f64] = "f64",
  [anon_sym_bytes] = "bytes",
  [sym_digit] = "digit",
  [anon_sym_DOT_DOT] = "..",
  [anon_sym_PIPE] = "|",
  [anon_sym_SLASH] = "/",
  [anon_sym_SLASH2] = "/",
  [sym_regex_pattern] = "regex_pattern",
  [anon_sym_COLON] = ":",
  [anon_sym_LBRACK_RBRACK] = "[]",
  [anon_sym_AMP] = "&",
  [sym_source_file] = "source_file",
  [sym_global_annotation_or_empty] = "global_annotation_or_empty",
  [sym_global_annotation] = "global_annotation",
  [sym_statement] = "statement",
  [sym_annotation] = "annotation",
  [sym_define] = "define",
  [sym_sample_type] = "sample_type",
  [sym_integer_range] = "integer_range",
  [sym_integer_expr] = "integer_expr",
  [sym_integer_type] = "integer_type",
  [sym_string_type] = "string_type",
  [sym_composite_field] = "composite_field",
  [sym_call_type] = "call_type",
  [sym_alias_type] = "alias_type",
  [aux_sym_source_file_repeat1] = "source_file_repeat1",
  [aux_sym_source_file_repeat2] = "source_file_repeat2",
  [aux_sym_statement_repeat1] = "statement_repeat1",
  [aux_sym_integer_expr_repeat1] = "integer_expr_repeat1",
  [aux_sym_integer_type_repeat1] = "integer_type_repeat1",
};

static const TSSymbol ts_symbol_map[] = {
  [ts_builtin_sym_end] = ts_builtin_sym_end,
  [sym_eol] = sym_eol,
  [aux_sym_global_annotation_token1] = aux_sym_global_annotation_token1,
  [anon_sym_EQ] = anon_sym_EQ,
  [sym_ident] = sym_ident,
  [aux_sym_annotation_token1] = aux_sym_annotation_token1,
  [anon_sym_f32] = anon_sym_f32,
  [anon_sym_f64] = anon_sym_f64,
  [anon_sym_bytes] = anon_sym_bytes,
  [sym_digit] = sym_digit,
  [anon_sym_DOT_DOT] = anon_sym_DOT_DOT,
  [anon_sym_PIPE] = anon_sym_PIPE,
  [anon_sym_SLASH] = anon_sym_SLASH,
  [anon_sym_SLASH2] = anon_sym_SLASH,
  [sym_regex_pattern] = sym_regex_pattern,
  [anon_sym_COLON] = anon_sym_COLON,
  [anon_sym_LBRACK_RBRACK] = anon_sym_LBRACK_RBRACK,
  [anon_sym_AMP] = anon_sym_AMP,
  [sym_source_file] = sym_source_file,
  [sym_global_annotation_or_empty] = sym_global_annotation_or_empty,
  [sym_global_annotation] = sym_global_annotation,
  [sym_statement] = sym_statement,
  [sym_annotation] = sym_annotation,
  [sym_define] = sym_define,
  [sym_sample_type] = sym_sample_type,
  [sym_integer_range] = sym_integer_range,
  [sym_integer_expr] = sym_integer_expr,
  [sym_integer_type] = sym_integer_type,
  [sym_string_type] = sym_string_type,
  [sym_composite_field] = sym_composite_field,
  [sym_call_type] = sym_call_type,
  [sym_alias_type] = sym_alias_type,
  [aux_sym_source_file_repeat1] = aux_sym_source_file_repeat1,
  [aux_sym_source_file_repeat2] = aux_sym_source_file_repeat2,
  [aux_sym_statement_repeat1] = aux_sym_statement_repeat1,
  [aux_sym_integer_expr_repeat1] = aux_sym_integer_expr_repeat1,
  [aux_sym_integer_type_repeat1] = aux_sym_integer_type_repeat1,
};

static const TSSymbolMetadata ts_symbol_metadata[] = {
  [ts_builtin_sym_end] = {
    .visible = false,
    .named = true,
  },
  [sym_eol] = {
    .visible = true,
    .named = true,
  },
  [aux_sym_global_annotation_token1] = {
    .visible = false,
    .named = false,
  },
  [anon_sym_EQ] = {
    .visible = true,
    .named = false,
  },
  [sym_ident] = {
    .visible = true,
    .named = true,
  },
  [aux_sym_annotation_token1] = {
    .visible = false,
    .named = false,
  },
  [anon_sym_f32] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_f64] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_bytes] = {
    .visible = true,
    .named = false,
  },
  [sym_digit] = {
    .visible = true,
    .named = true,
  },
  [anon_sym_DOT_DOT] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_PIPE] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_SLASH] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_SLASH2] = {
    .visible = true,
    .named = false,
  },
  [sym_regex_pattern] = {
    .visible = true,
    .named = true,
  },
  [anon_sym_COLON] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_LBRACK_RBRACK] = {
    .visible = true,
    .named = false,
  },
  [anon_sym_AMP] = {
    .visible = true,
    .named = false,
  },
  [sym_source_file] = {
    .visible = true,
    .named = true,
  },
  [sym_global_annotation_or_empty] = {
    .visible = true,
    .named = true,
  },
  [sym_global_annotation] = {
    .visible = true,
    .named = true,
  },
  [sym_statement] = {
    .visible = true,
    .named = true,
  },
  [sym_annotation] = {
    .visible = true,
    .named = true,
  },
  [sym_define] = {
    .visible = true,
    .named = true,
  },
  [sym_sample_type] = {
    .visible = true,
    .named = true,
  },
  [sym_integer_range] = {
    .visible = true,
    .named = true,
  },
  [sym_integer_expr] = {
    .visible = true,
    .named = true,
  },
  [sym_integer_type] = {
    .visible = true,
    .named = true,
  },
  [sym_string_type] = {
    .visible = true,
    .named = true,
  },
  [sym_composite_field] = {
    .visible = true,
    .named = true,
  },
  [sym_call_type] = {
    .visible = true,
    .named = true,
  },
  [sym_alias_type] = {
    .visible = true,
    .named = true,
  },
  [aux_sym_source_file_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_source_file_repeat2] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_statement_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_integer_expr_repeat1] = {
    .visible = false,
    .named = false,
  },
  [aux_sym_integer_type_repeat1] = {
    .visible = false,
    .named = false,
  },
};

enum {
  field_name = 1,
  field_pattern = 2,
  field_value = 3,
};

static const char * const ts_field_names[] = {
  [0] = NULL,
  [field_name] = "name",
  [field_pattern] = "pattern",
  [field_value] = "value",
};

static const TSFieldMapSlice ts_field_map_slices[PRODUCTION_ID_COUNT] = {
  [1] = {.index = 0, .length = 2},
  [2] = {.index = 2, .length = 1},
};

static const TSFieldMapEntry ts_field_map_entries[] = {
  [0] =
    {field_name, 0},
    {field_value, 2},
  [2] =
    {field_pattern, 1},
};

static const TSSymbol ts_alias_sequences[PRODUCTION_ID_COUNT][MAX_ALIAS_SEQUENCE_LENGTH] = {
  [0] = {0},
};

static const uint16_t ts_non_terminal_alias_map[] = {
  0,
};

static const TSStateId ts_primary_state_ids[STATE_COUNT] = {
  [0] = 0,
  [1] = 1,
  [2] = 2,
  [3] = 3,
  [4] = 4,
  [5] = 5,
  [6] = 6,
  [7] = 7,
  [8] = 8,
  [9] = 9,
  [10] = 10,
  [11] = 11,
  [12] = 12,
  [13] = 13,
  [14] = 14,
  [15] = 15,
  [16] = 16,
  [17] = 17,
  [18] = 18,
  [19] = 19,
  [20] = 20,
  [21] = 21,
  [22] = 22,
  [23] = 23,
  [24] = 24,
  [25] = 25,
  [26] = 26,
  [27] = 27,
  [28] = 13,
  [29] = 29,
  [30] = 30,
  [31] = 31,
  [32] = 32,
  [33] = 33,
  [34] = 34,
  [35] = 35,
  [36] = 36,
  [37] = 37,
  [38] = 38,
  [39] = 39,
  [40] = 40,
  [41] = 41,
  [42] = 42,
  [43] = 43,
  [44] = 44,
  [45] = 45,
  [46] = 46,
  [47] = 47,
  [48] = 48,
};

static bool ts_lex(TSLexer *lexer, TSStateId state) {
  START_LEXER();
  eof = lexer->eof(lexer);
  switch (state) {
    case 0:
      if (eof) ADVANCE(15);
      if (lookahead == '&') ADVANCE(40);
      if (lookahead == '.') ADVANCE(1);
      if (lookahead == '/') ADVANCE(36);
      if (lookahead == ':') ADVANCE(38);
      if (lookahead == '=') ADVANCE(18);
      if (lookahead == '[') ADVANCE(7);
      if (lookahead == 'b') ADVANCE(25);
      if (lookahead == 'f') ADVANCE(20);
      if (lookahead == '|') ADVANCE(34);
      if (lookahead == '+' ||
          lookahead == '-') ADVANCE(8);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(13)
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(32);
      if (('A' <= lookahead && lookahead <= 'Z') ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(26);
      END_STATE();
    case 1:
      if (lookahead == '.') ADVANCE(33);
      END_STATE();
    case 2:
      if (lookahead == '/') ADVANCE(36);
      if (lookahead == '[') ADVANCE(6);
      if (lookahead == '\\') ADVANCE(10);
      if (lookahead != 0 &&
          lookahead != '\n') ADVANCE(37);
      END_STATE();
    case 3:
      if (lookahead == '/') ADVANCE(35);
      if (lookahead == 'b') ADVANCE(25);
      if (lookahead == 'f') ADVANCE(20);
      if (lookahead == '+' ||
          lookahead == '-') ADVANCE(8);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(3)
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(32);
      if (('A' <= lookahead && lookahead <= 'Z') ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(26);
      END_STATE();
    case 4:
      if (lookahead == '/') ADVANCE(27);
      END_STATE();
    case 5:
      if (lookahead == '/') ADVANCE(28);
      END_STATE();
    case 6:
      if (lookahead == '\\') ADVANCE(9);
      if (lookahead == ']') ADVANCE(37);
      if (lookahead != 0 &&
          lookahead != '\n') ADVANCE(6);
      END_STATE();
    case 7:
      if (lookahead == ']') ADVANCE(39);
      END_STATE();
    case 8:
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(32);
      END_STATE();
    case 9:
      if (lookahead != 0 &&
          lookahead != '\n') ADVANCE(6);
      END_STATE();
    case 10:
      if (lookahead != 0 &&
          lookahead != '\n') ADVANCE(37);
      END_STATE();
    case 11:
      if (eof) ADVANCE(15);
      if (lookahead == '\n') ADVANCE(16);
      if (lookahead == '/') ADVANCE(4);
      if (lookahead == ':') ADVANCE(38);
      if (lookahead == '[') ADVANCE(7);
      if (lookahead == '|') ADVANCE(34);
      if (lookahead == '\t' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(11)
      if (('A' <= lookahead && lookahead <= 'Z') ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(26);
      END_STATE();
    case 12:
      if (eof) ADVANCE(15);
      if (lookahead == '\n') ADVANCE(16);
      if (lookahead == '/') ADVANCE(5);
      if (lookahead == '[') ADVANCE(7);
      if (lookahead == '|') ADVANCE(34);
      if (lookahead == '\t' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(12)
      if (('A' <= lookahead && lookahead <= 'Z') ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(26);
      END_STATE();
    case 13:
      if (eof) ADVANCE(15);
      if (lookahead == '&') ADVANCE(40);
      if (lookahead == '.') ADVANCE(1);
      if (lookahead == '/') ADVANCE(35);
      if (lookahead == ':') ADVANCE(38);
      if (lookahead == '=') ADVANCE(18);
      if (lookahead == '[') ADVANCE(7);
      if (lookahead == 'b') ADVANCE(25);
      if (lookahead == 'f') ADVANCE(20);
      if (lookahead == '|') ADVANCE(34);
      if (lookahead == '+' ||
          lookahead == '-') ADVANCE(8);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(13)
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(32);
      if (('A' <= lookahead && lookahead <= 'Z') ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(26);
      END_STATE();
    case 14:
      if (eof) ADVANCE(15);
      if (lookahead == '/') ADVANCE(5);
      if (lookahead == '\t' ||
          lookahead == '\n' ||
          lookahead == '\r' ||
          lookahead == ' ') SKIP(14)
      if (('A' <= lookahead && lookahead <= 'Z') ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(26);
      END_STATE();
    case 15:
      ACCEPT_TOKEN(ts_builtin_sym_end);
      END_STATE();
    case 16:
      ACCEPT_TOKEN(sym_eol);
      if (lookahead == '\n') ADVANCE(16);
      END_STATE();
    case 17:
      ACCEPT_TOKEN(aux_sym_global_annotation_token1);
      if (lookahead != 0 &&
          lookahead != '\n') ADVANCE(17);
      END_STATE();
    case 18:
      ACCEPT_TOKEN(anon_sym_EQ);
      END_STATE();
    case 19:
      ACCEPT_TOKEN(sym_ident);
      if (lookahead == '2') ADVANCE(29);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(26);
      END_STATE();
    case 20:
      ACCEPT_TOKEN(sym_ident);
      if (lookahead == '3') ADVANCE(19);
      if (lookahead == '6') ADVANCE(21);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(26);
      END_STATE();
    case 21:
      ACCEPT_TOKEN(sym_ident);
      if (lookahead == '4') ADVANCE(30);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(26);
      END_STATE();
    case 22:
      ACCEPT_TOKEN(sym_ident);
      if (lookahead == 'e') ADVANCE(23);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(26);
      END_STATE();
    case 23:
      ACCEPT_TOKEN(sym_ident);
      if (lookahead == 's') ADVANCE(31);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(26);
      END_STATE();
    case 24:
      ACCEPT_TOKEN(sym_ident);
      if (lookahead == 't') ADVANCE(22);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(26);
      END_STATE();
    case 25:
      ACCEPT_TOKEN(sym_ident);
      if (lookahead == 'y') ADVANCE(24);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(26);
      END_STATE();
    case 26:
      ACCEPT_TOKEN(sym_ident);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(26);
      END_STATE();
    case 27:
      ACCEPT_TOKEN(aux_sym_annotation_token1);
      if (lookahead == '/') ADVANCE(17);
      if (lookahead != 0 &&
          lookahead != '\n') ADVANCE(28);
      END_STATE();
    case 28:
      ACCEPT_TOKEN(aux_sym_annotation_token1);
      if (lookahead != 0 &&
          lookahead != '\n') ADVANCE(28);
      END_STATE();
    case 29:
      ACCEPT_TOKEN(anon_sym_f32);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(26);
      END_STATE();
    case 30:
      ACCEPT_TOKEN(anon_sym_f64);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(26);
      END_STATE();
    case 31:
      ACCEPT_TOKEN(anon_sym_bytes);
      if (('0' <= lookahead && lookahead <= '9') ||
          ('A' <= lookahead && lookahead <= 'Z') ||
          lookahead == '_' ||
          ('a' <= lookahead && lookahead <= 'z')) ADVANCE(26);
      END_STATE();
    case 32:
      ACCEPT_TOKEN(sym_digit);
      if (('0' <= lookahead && lookahead <= '9')) ADVANCE(32);
      END_STATE();
    case 33:
      ACCEPT_TOKEN(anon_sym_DOT_DOT);
      END_STATE();
    case 34:
      ACCEPT_TOKEN(anon_sym_PIPE);
      END_STATE();
    case 35:
      ACCEPT_TOKEN(anon_sym_SLASH);
      END_STATE();
    case 36:
      ACCEPT_TOKEN(anon_sym_SLASH2);
      END_STATE();
    case 37:
      ACCEPT_TOKEN(sym_regex_pattern);
      if (lookahead == '[') ADVANCE(6);
      if (lookahead == '\\') ADVANCE(10);
      if (lookahead != 0 &&
          lookahead != '\n' &&
          lookahead != '/') ADVANCE(37);
      END_STATE();
    case 38:
      ACCEPT_TOKEN(anon_sym_COLON);
      END_STATE();
    case 39:
      ACCEPT_TOKEN(anon_sym_LBRACK_RBRACK);
      END_STATE();
    case 40:
      ACCEPT_TOKEN(anon_sym_AMP);
      END_STATE();
    default:
      return false;
  }
}

static const TSLexMode ts_lex_modes[STATE_COUNT] = {
  [0] = {.lex_state = 0},
  [1] = {.lex_state = 11},
  [2] = {.lex_state = 3},
  [3] = {.lex_state = 3},
  [4] = {.lex_state = 11},
  [5] = {.lex_state = 12},
  [6] = {.lex_state = 12},
  [7] = {.lex_state = 12},
  [8] = {.lex_state = 11},
  [9] = {.lex_state = 14},
  [10] = {.lex_state = 14},
  [11] = {.lex_state = 14},
  [12] = {.lex_state = 12},
  [13] = {.lex_state = 12},
  [14] = {.lex_state = 12},
  [15] = {.lex_state = 12},
  [16] = {.lex_state = 11},
  [17] = {.lex_state = 12},
  [18] = {.lex_state = 12},
  [19] = {.lex_state = 12},
  [20] = {.lex_state = 12},
  [21] = {.lex_state = 12},
  [22] = {.lex_state = 11},
  [23] = {.lex_state = 14},
  [24] = {.lex_state = 12},
  [25] = {.lex_state = 14},
  [26] = {.lex_state = 14},
  [27] = {.lex_state = 14},
  [28] = {.lex_state = 11},
  [29] = {.lex_state = 11},
  [30] = {.lex_state = 11},
  [31] = {.lex_state = 14},
  [32] = {.lex_state = 14},
  [33] = {.lex_state = 14},
  [34] = {.lex_state = 14},
  [35] = {.lex_state = 0},
  [36] = {.lex_state = 0},
  [37] = {.lex_state = 14},
  [38] = {.lex_state = 14},
  [39] = {.lex_state = 0},
  [40] = {.lex_state = 2},
  [41] = {.lex_state = 0},
  [42] = {.lex_state = 11},
  [43] = {.lex_state = 0},
  [44] = {.lex_state = 11},
  [45] = {.lex_state = 0},
  [46] = {.lex_state = 11},
  [47] = {.lex_state = 2},
  [48] = {.lex_state = 0},
};

static const uint16_t ts_parse_table[LARGE_STATE_COUNT][SYMBOL_COUNT] = {
  [0] = {
    [ts_builtin_sym_end] = ACTIONS(1),
    [anon_sym_EQ] = ACTIONS(1),
    [sym_ident] = ACTIONS(1),
    [anon_sym_f32] = ACTIONS(1),
    [anon_sym_f64] = ACTIONS(1),
    [anon_sym_bytes] = ACTIONS(1),
    [sym_digit] = ACTIONS(1),
    [anon_sym_DOT_DOT] = ACTIONS(1),
    [anon_sym_PIPE] = ACTIONS(1),
    [anon_sym_SLASH] = ACTIONS(1),
    [anon_sym_SLASH2] = ACTIONS(1),
    [anon_sym_COLON] = ACTIONS(1),
    [anon_sym_LBRACK_RBRACK] = ACTIONS(1),
    [anon_sym_AMP] = ACTIONS(1),
  },
  [1] = {
    [sym_source_file] = STATE(39),
    [sym_global_annotation_or_empty] = STATE(4),
    [sym_global_annotation] = STATE(22),
    [sym_statement] = STATE(10),
    [sym_annotation] = STATE(25),
    [aux_sym_source_file_repeat1] = STATE(4),
    [aux_sym_source_file_repeat2] = STATE(10),
    [aux_sym_statement_repeat1] = STATE(25),
    [ts_builtin_sym_end] = ACTIONS(3),
    [sym_eol] = ACTIONS(5),
    [aux_sym_global_annotation_token1] = ACTIONS(7),
    [sym_ident] = ACTIONS(9),
    [aux_sym_annotation_token1] = ACTIONS(11),
  },
};

static const uint16_t ts_small_parse_table[] = {
  [0] = 8,
    ACTIONS(13), 1,
      sym_ident,
    ACTIONS(17), 1,
      sym_digit,
    ACTIONS(19), 1,
      anon_sym_SLASH,
    STATE(7), 1,
      sym_integer_range,
    STATE(26), 1,
      sym_define,
    STATE(42), 1,
      sym_call_type,
    ACTIONS(15), 3,
      anon_sym_f32,
      anon_sym_f64,
      anon_sym_bytes,
    STATE(31), 5,
      sym_sample_type,
      sym_integer_type,
      sym_string_type,
      sym_composite_field,
      sym_alias_type,
  [31] = 8,
    ACTIONS(13), 1,
      sym_ident,
    ACTIONS(17), 1,
      sym_digit,
    ACTIONS(19), 1,
      anon_sym_SLASH,
    STATE(7), 1,
      sym_integer_range,
    STATE(27), 1,
      sym_define,
    STATE(42), 1,
      sym_call_type,
    ACTIONS(15), 3,
      anon_sym_f32,
      anon_sym_f64,
      anon_sym_bytes,
    STATE(31), 5,
      sym_sample_type,
      sym_integer_type,
      sym_string_type,
      sym_composite_field,
      sym_alias_type,
  [62] = 9,
    ACTIONS(5), 1,
      sym_eol,
    ACTIONS(7), 1,
      aux_sym_global_annotation_token1,
    ACTIONS(9), 1,
      sym_ident,
    ACTIONS(11), 1,
      aux_sym_annotation_token1,
    ACTIONS(21), 1,
      ts_builtin_sym_end,
    STATE(22), 1,
      sym_global_annotation,
    STATE(8), 2,
      sym_global_annotation_or_empty,
      aux_sym_source_file_repeat1,
    STATE(11), 2,
      sym_statement,
      aux_sym_source_file_repeat2,
    STATE(25), 2,
      sym_annotation,
      aux_sym_statement_repeat1,
  [93] = 6,
    ACTIONS(23), 1,
      ts_builtin_sym_end,
    ACTIONS(25), 1,
      sym_eol,
    ACTIONS(30), 1,
      anon_sym_PIPE,
    STATE(29), 1,
      aux_sym_integer_expr_repeat1,
    ACTIONS(28), 2,
      sym_ident,
      aux_sym_annotation_token1,
    STATE(5), 2,
      sym_integer_expr,
      aux_sym_integer_type_repeat1,
  [114] = 6,
    ACTIONS(33), 1,
      ts_builtin_sym_end,
    ACTIONS(35), 1,
      sym_eol,
    ACTIONS(39), 1,
      anon_sym_PIPE,
    STATE(12), 1,
      aux_sym_integer_expr_repeat1,
    ACTIONS(37), 2,
      sym_ident,
      aux_sym_annotation_token1,
    STATE(5), 2,
      sym_integer_expr,
      aux_sym_integer_type_repeat1,
  [135] = 6,
    ACTIONS(39), 1,
      anon_sym_PIPE,
    ACTIONS(41), 1,
      ts_builtin_sym_end,
    ACTIONS(43), 1,
      sym_eol,
    STATE(14), 1,
      aux_sym_integer_expr_repeat1,
    ACTIONS(45), 2,
      sym_ident,
      aux_sym_annotation_token1,
    STATE(6), 2,
      sym_integer_expr,
      aux_sym_integer_type_repeat1,
  [156] = 6,
    ACTIONS(47), 1,
      ts_builtin_sym_end,
    ACTIONS(49), 1,
      sym_eol,
    ACTIONS(52), 1,
      aux_sym_global_annotation_token1,
    STATE(22), 1,
      sym_global_annotation,
    ACTIONS(55), 2,
      sym_ident,
      aux_sym_annotation_token1,
    STATE(8), 2,
      sym_global_annotation_or_empty,
      aux_sym_source_file_repeat1,
  [177] = 5,
    ACTIONS(57), 1,
      ts_builtin_sym_end,
    ACTIONS(59), 1,
      sym_ident,
    ACTIONS(62), 1,
      aux_sym_annotation_token1,
    STATE(9), 2,
      sym_statement,
      aux_sym_source_file_repeat2,
    STATE(25), 2,
      sym_annotation,
      aux_sym_statement_repeat1,
  [195] = 5,
    ACTIONS(21), 1,
      ts_builtin_sym_end,
    ACTIONS(65), 1,
      sym_ident,
    ACTIONS(67), 1,
      aux_sym_annotation_token1,
    STATE(9), 2,
      sym_statement,
      aux_sym_source_file_repeat2,
    STATE(25), 2,
      sym_annotation,
      aux_sym_statement_repeat1,
  [213] = 5,
    ACTIONS(65), 1,
      sym_ident,
    ACTIONS(67), 1,
      aux_sym_annotation_token1,
    ACTIONS(69), 1,
      ts_builtin_sym_end,
    STATE(9), 2,
      sym_statement,
      aux_sym_source_file_repeat2,
    STATE(25), 2,
      sym_annotation,
      aux_sym_statement_repeat1,
  [231] = 5,
    ACTIONS(71), 1,
      ts_builtin_sym_end,
    ACTIONS(73), 1,
      sym_eol,
    ACTIONS(77), 1,
      anon_sym_PIPE,
    STATE(13), 1,
      aux_sym_integer_expr_repeat1,
    ACTIONS(75), 2,
      sym_ident,
      aux_sym_annotation_token1,
  [248] = 4,
    ACTIONS(79), 1,
      ts_builtin_sym_end,
    ACTIONS(81), 1,
      sym_eol,
    STATE(13), 1,
      aux_sym_integer_expr_repeat1,
    ACTIONS(84), 3,
      sym_ident,
      aux_sym_annotation_token1,
      anon_sym_PIPE,
  [263] = 5,
    ACTIONS(33), 1,
      ts_builtin_sym_end,
    ACTIONS(73), 1,
      sym_eol,
    ACTIONS(77), 1,
      anon_sym_PIPE,
    STATE(13), 1,
      aux_sym_integer_expr_repeat1,
    ACTIONS(37), 2,
      sym_ident,
      aux_sym_annotation_token1,
  [280] = 3,
    ACTIONS(90), 1,
      anon_sym_LBRACK_RBRACK,
    ACTIONS(86), 2,
      ts_builtin_sym_end,
      sym_eol,
    ACTIONS(88), 2,
      sym_ident,
      aux_sym_annotation_token1,
  [292] = 2,
    ACTIONS(92), 2,
      ts_builtin_sym_end,
      sym_eol,
    ACTIONS(94), 3,
      aux_sym_global_annotation_token1,
      sym_ident,
      aux_sym_annotation_token1,
  [302] = 2,
    ACTIONS(96), 2,
      ts_builtin_sym_end,
      sym_eol,
    ACTIONS(98), 3,
      sym_ident,
      aux_sym_annotation_token1,
      anon_sym_PIPE,
  [312] = 4,
    ACTIONS(73), 1,
      sym_eol,
    ACTIONS(100), 1,
      ts_builtin_sym_end,
    STATE(13), 1,
      aux_sym_integer_expr_repeat1,
    ACTIONS(102), 2,
      sym_ident,
      aux_sym_annotation_token1,
  [326] = 2,
    ACTIONS(104), 2,
      ts_builtin_sym_end,
      sym_eol,
    ACTIONS(106), 3,
      sym_ident,
      aux_sym_annotation_token1,
      anon_sym_PIPE,
  [336] = 2,
    ACTIONS(108), 2,
      ts_builtin_sym_end,
      sym_eol,
    ACTIONS(110), 3,
      sym_ident,
      aux_sym_annotation_token1,
      anon_sym_PIPE,
  [346] = 4,
    ACTIONS(112), 1,
      ts_builtin_sym_end,
    ACTIONS(114), 1,
      sym_eol,
    STATE(18), 1,
      aux_sym_integer_expr_repeat1,
    ACTIONS(116), 2,
      sym_ident,
      aux_sym_annotation_token1,
  [360] = 2,
    ACTIONS(118), 2,
      ts_builtin_sym_end,
      sym_eol,
    ACTIONS(120), 3,
      aux_sym_global_annotation_token1,
      sym_ident,
      aux_sym_annotation_token1,
  [370] = 3,
    ACTIONS(122), 1,
      sym_ident,
    ACTIONS(124), 1,
      aux_sym_annotation_token1,
    STATE(23), 2,
      sym_annotation,
      aux_sym_statement_repeat1,
  [381] = 2,
    ACTIONS(127), 2,
      ts_builtin_sym_end,
      sym_eol,
    ACTIONS(129), 2,
      sym_ident,
      aux_sym_annotation_token1,
  [390] = 3,
    ACTIONS(67), 1,
      aux_sym_annotation_token1,
    ACTIONS(131), 1,
      sym_ident,
    STATE(23), 2,
      sym_annotation,
      aux_sym_statement_repeat1,
  [401] = 1,
    ACTIONS(133), 3,
      ts_builtin_sym_end,
      sym_ident,
      aux_sym_annotation_token1,
  [407] = 1,
    ACTIONS(135), 3,
      ts_builtin_sym_end,
      sym_ident,
      aux_sym_annotation_token1,
  [413] = 3,
    ACTIONS(84), 1,
      anon_sym_PIPE,
    ACTIONS(137), 1,
      sym_eol,
    STATE(28), 1,
      aux_sym_integer_expr_repeat1,
  [423] = 3,
    ACTIONS(77), 1,
      anon_sym_PIPE,
    ACTIONS(140), 1,
      sym_eol,
    STATE(28), 1,
      aux_sym_integer_expr_repeat1,
  [433] = 3,
    ACTIONS(86), 1,
      sym_eol,
    ACTIONS(90), 1,
      anon_sym_LBRACK_RBRACK,
    ACTIONS(142), 1,
      anon_sym_COLON,
  [443] = 1,
    ACTIONS(144), 3,
      ts_builtin_sym_end,
      sym_ident,
      aux_sym_annotation_token1,
  [449] = 1,
    ACTIONS(146), 3,
      ts_builtin_sym_end,
      sym_ident,
      aux_sym_annotation_token1,
  [455] = 1,
    ACTIONS(148), 3,
      ts_builtin_sym_end,
      sym_ident,
      aux_sym_annotation_token1,
  [461] = 1,
    ACTIONS(150), 3,
      ts_builtin_sym_end,
      sym_ident,
      aux_sym_annotation_token1,
  [467] = 2,
    ACTIONS(17), 1,
      sym_digit,
    STATE(17), 1,
      sym_integer_range,
  [474] = 2,
    ACTIONS(17), 1,
      sym_digit,
    STATE(19), 1,
      sym_integer_range,
  [481] = 2,
    ACTIONS(152), 1,
      sym_ident,
    STATE(21), 1,
      sym_call_type,
  [488] = 1,
    ACTIONS(154), 2,
      sym_ident,
      aux_sym_annotation_token1,
  [493] = 1,
    ACTIONS(156), 1,
      ts_builtin_sym_end,
  [497] = 1,
    ACTIONS(158), 1,
      sym_regex_pattern,
  [501] = 1,
    ACTIONS(160), 1,
      anon_sym_DOT_DOT,
  [505] = 1,
    ACTIONS(162), 1,
      sym_eol,
  [509] = 1,
    ACTIONS(164), 1,
      anon_sym_EQ,
  [513] = 1,
    ACTIONS(166), 1,
      sym_eol,
  [517] = 1,
    ACTIONS(168), 1,
      anon_sym_EQ,
  [521] = 1,
    ACTIONS(170), 1,
      sym_eol,
  [525] = 1,
    ACTIONS(172), 1,
      anon_sym_SLASH2,
  [529] = 1,
    ACTIONS(174), 1,
      sym_digit,
};

static const uint32_t ts_small_parse_table_map[] = {
  [SMALL_STATE(2)] = 0,
  [SMALL_STATE(3)] = 31,
  [SMALL_STATE(4)] = 62,
  [SMALL_STATE(5)] = 93,
  [SMALL_STATE(6)] = 114,
  [SMALL_STATE(7)] = 135,
  [SMALL_STATE(8)] = 156,
  [SMALL_STATE(9)] = 177,
  [SMALL_STATE(10)] = 195,
  [SMALL_STATE(11)] = 213,
  [SMALL_STATE(12)] = 231,
  [SMALL_STATE(13)] = 248,
  [SMALL_STATE(14)] = 263,
  [SMALL_STATE(15)] = 280,
  [SMALL_STATE(16)] = 292,
  [SMALL_STATE(17)] = 302,
  [SMALL_STATE(18)] = 312,
  [SMALL_STATE(19)] = 326,
  [SMALL_STATE(20)] = 336,
  [SMALL_STATE(21)] = 346,
  [SMALL_STATE(22)] = 360,
  [SMALL_STATE(23)] = 370,
  [SMALL_STATE(24)] = 381,
  [SMALL_STATE(25)] = 390,
  [SMALL_STATE(26)] = 401,
  [SMALL_STATE(27)] = 407,
  [SMALL_STATE(28)] = 413,
  [SMALL_STATE(29)] = 423,
  [SMALL_STATE(30)] = 433,
  [SMALL_STATE(31)] = 443,
  [SMALL_STATE(32)] = 449,
  [SMALL_STATE(33)] = 455,
  [SMALL_STATE(34)] = 461,
  [SMALL_STATE(35)] = 467,
  [SMALL_STATE(36)] = 474,
  [SMALL_STATE(37)] = 481,
  [SMALL_STATE(38)] = 488,
  [SMALL_STATE(39)] = 493,
  [SMALL_STATE(40)] = 497,
  [SMALL_STATE(41)] = 501,
  [SMALL_STATE(42)] = 505,
  [SMALL_STATE(43)] = 509,
  [SMALL_STATE(44)] = 513,
  [SMALL_STATE(45)] = 517,
  [SMALL_STATE(46)] = 521,
  [SMALL_STATE(47)] = 525,
  [SMALL_STATE(48)] = 529,
};

static const TSParseActionEntry ts_parse_actions[] = {
  [0] = {.entry = {.count = 0, .reusable = false}},
  [1] = {.entry = {.count = 1, .reusable = false}}, RECOVER(),
  [3] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_source_file, 0),
  [5] = {.entry = {.count = 1, .reusable = true}}, SHIFT(22),
  [7] = {.entry = {.count = 1, .reusable = false}}, SHIFT(46),
  [9] = {.entry = {.count = 1, .reusable = false}}, SHIFT(45),
  [11] = {.entry = {.count = 1, .reusable = false}}, SHIFT(44),
  [13] = {.entry = {.count = 1, .reusable = false}}, SHIFT(30),
  [15] = {.entry = {.count = 1, .reusable = false}}, SHIFT(32),
  [17] = {.entry = {.count = 1, .reusable = true}}, SHIFT(41),
  [19] = {.entry = {.count = 1, .reusable = true}}, SHIFT(40),
  [21] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_source_file, 1),
  [23] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_integer_type_repeat1, 2),
  [25] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_integer_type_repeat1, 2), SHIFT_REPEAT(29),
  [28] = {.entry = {.count = 1, .reusable = false}}, REDUCE(aux_sym_integer_type_repeat1, 2),
  [30] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_integer_type_repeat1, 2), SHIFT_REPEAT(36),
  [33] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_integer_type, 2),
  [35] = {.entry = {.count = 1, .reusable = true}}, SHIFT(12),
  [37] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_integer_type, 2),
  [39] = {.entry = {.count = 1, .reusable = false}}, SHIFT(36),
  [41] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_integer_type, 1),
  [43] = {.entry = {.count = 1, .reusable = true}}, SHIFT(14),
  [45] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_integer_type, 1),
  [47] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2),
  [49] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(22),
  [52] = {.entry = {.count = 2, .reusable = false}}, REDUCE(aux_sym_source_file_repeat1, 2), SHIFT_REPEAT(46),
  [55] = {.entry = {.count = 1, .reusable = false}}, REDUCE(aux_sym_source_file_repeat1, 2),
  [57] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_source_file_repeat2, 2),
  [59] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat2, 2), SHIFT_REPEAT(45),
  [62] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_source_file_repeat2, 2), SHIFT_REPEAT(44),
  [65] = {.entry = {.count = 1, .reusable = true}}, SHIFT(45),
  [67] = {.entry = {.count = 1, .reusable = true}}, SHIFT(44),
  [69] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_source_file, 2),
  [71] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_integer_type, 3),
  [73] = {.entry = {.count = 1, .reusable = true}}, SHIFT(13),
  [75] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_integer_type, 3),
  [77] = {.entry = {.count = 1, .reusable = false}}, SHIFT(35),
  [79] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_integer_expr_repeat1, 2),
  [81] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_integer_expr_repeat1, 2), SHIFT_REPEAT(13),
  [84] = {.entry = {.count = 1, .reusable = false}}, REDUCE(aux_sym_integer_expr_repeat1, 2),
  [86] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_call_type, 1),
  [88] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_call_type, 1),
  [90] = {.entry = {.count = 1, .reusable = false}}, SHIFT(24),
  [92] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_global_annotation, 2),
  [94] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_global_annotation, 2),
  [96] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_integer_expr, 3),
  [98] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_integer_expr, 3),
  [100] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_composite_field, 4, .production_id = 1),
  [102] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_composite_field, 4, .production_id = 1),
  [104] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_integer_expr, 2),
  [106] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_integer_expr, 2),
  [108] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_integer_range, 3),
  [110] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_integer_range, 3),
  [112] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_composite_field, 3, .production_id = 1),
  [114] = {.entry = {.count = 1, .reusable = true}}, SHIFT(18),
  [116] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_composite_field, 3, .production_id = 1),
  [118] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_global_annotation_or_empty, 1),
  [120] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_global_annotation_or_empty, 1),
  [122] = {.entry = {.count = 1, .reusable = true}}, REDUCE(aux_sym_statement_repeat1, 2),
  [124] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_statement_repeat1, 2), SHIFT_REPEAT(44),
  [127] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_call_type, 2),
  [129] = {.entry = {.count = 1, .reusable = false}}, REDUCE(sym_call_type, 2),
  [131] = {.entry = {.count = 1, .reusable = true}}, SHIFT(43),
  [133] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_statement, 4),
  [135] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_statement, 3),
  [137] = {.entry = {.count = 2, .reusable = true}}, REDUCE(aux_sym_integer_expr_repeat1, 2), SHIFT_REPEAT(28),
  [140] = {.entry = {.count = 1, .reusable = true}}, SHIFT(28),
  [142] = {.entry = {.count = 1, .reusable = false}}, SHIFT(37),
  [144] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_define, 1),
  [146] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_sample_type, 1),
  [148] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_string_type, 3, .production_id = 2),
  [150] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_alias_type, 2),
  [152] = {.entry = {.count = 1, .reusable = true}}, SHIFT(15),
  [154] = {.entry = {.count = 1, .reusable = true}}, REDUCE(sym_annotation, 2),
  [156] = {.entry = {.count = 1, .reusable = true}},  ACCEPT_INPUT(),
  [158] = {.entry = {.count = 1, .reusable = true}}, SHIFT(47),
  [160] = {.entry = {.count = 1, .reusable = true}}, SHIFT(48),
  [162] = {.entry = {.count = 1, .reusable = true}}, SHIFT(34),
  [164] = {.entry = {.count = 1, .reusable = true}}, SHIFT(2),
  [166] = {.entry = {.count = 1, .reusable = true}}, SHIFT(38),
  [168] = {.entry = {.count = 1, .reusable = true}}, SHIFT(3),
  [170] = {.entry = {.count = 1, .reusable = true}}, SHIFT(16),
  [172] = {.entry = {.count = 1, .reusable = true}}, SHIFT(33),
  [174] = {.entry = {.count = 1, .reusable = true}}, SHIFT(20),
};

#ifdef __cplusplus
extern "C" {
#endif
#ifdef _WIN32
#define extern __declspec(dllexport)
#endif

extern const TSLanguage *tree_sitter_TINY_DEFINE_LANGUAGE(void) {
  static const TSLanguage language = {
    .version = LANGUAGE_VERSION,
    .symbol_count = SYMBOL_COUNT,
    .alias_count = ALIAS_COUNT,
    .token_count = TOKEN_COUNT,
    .external_token_count = EXTERNAL_TOKEN_COUNT,
    .state_count = STATE_COUNT,
    .large_state_count = LARGE_STATE_COUNT,
    .production_id_count = PRODUCTION_ID_COUNT,
    .field_count = FIELD_COUNT,
    .max_alias_sequence_length = MAX_ALIAS_SEQUENCE_LENGTH,
    .parse_table = &ts_parse_table[0][0],
    .small_parse_table = ts_small_parse_table,
    .small_parse_table_map = ts_small_parse_table_map,
    .parse_actions = ts_parse_actions,
    .symbol_names = ts_symbol_names,
    .field_names = ts_field_names,
    .field_map_slices = ts_field_map_slices,
    .field_map_entries = ts_field_map_entries,
    .symbol_metadata = ts_symbol_metadata,
    .public_symbol_map = ts_symbol_map,
    .alias_map = ts_non_terminal_alias_map,
    .alias_sequences = &ts_alias_sequences[0][0],
    .lex_modes = ts_lex_modes,
    .lex_fn = ts_lex,
    .primary_state_ids = ts_primary_state_ids,
  };
  return &language;
}
#ifdef __cplusplus
}
#endif
