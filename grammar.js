module.exports = grammar({
  name: "TINY_DEFINE_LANGUAGE",

  rules: {
    source_file: ($) => seq(
      prec.left(repeat($.global_annotation_or_empty)),
      prec.left(repeat($.statement)),
    ),
    eol: ($) => "\n",
    global_annotation_or_empty: $ => prec.left(choice(
      $.global_annotation,
      $.eol,
    )),
    global_annotation: $ => seq(
      /\/\/\/[^\n]*/,
      $.eol,
    ),
    statement: ($) => seq(
      prec.left(repeat($.annotation)),
      $.ident,
      "=",
      $.define,
    ),
    ident: ($) => /[a-zA-Z][a-zA-Z_0-9]*/,
    annotation: ($) => seq(
      /\/\/[^\n]*/,
      $.eol
    ),
    define: ($) => choice(
      $.sample_type,
      $.integer_type,
      $.string_type,
      $.composite_field,
      $.alias_type,
    ),
    sample_type: ($) => choice(
      "f32",
      "f64",
      "bytes"
    ),
    digit: ($) => /[\+\-]?[0-9]+/,
    integer_range: ($) => seq(
      $.digit,
      "..",
      $.digit,
    ),
    integer_expr: ($) => seq(
      repeat($.eol),
      "|",
      $.integer_range,
    ),
    integer_type: ($) => prec.left(seq(
      $.integer_range,
      repeat($.integer_expr),
      repeat($.eol),
    )),
    string_type: ($) => seq(
      '/',
      field('pattern', $.regex_pattern),
      token.immediate('/'),
    ),
    regex_pattern: ($) => token.immediate(prec(-1,
      repeat1(choice(
        seq(
          '[',
          repeat(choice(
            seq('\\', /./), // escaped character
            /[^\]\n\\]/       // any character besides ']' or '\n'
          )),
          ']'
        ),              // square-bracket-delimited character class
        seq('\\', /./), // escaped character
        /[^/\\\[\n]/    // any character besides '[', '\', '/', '\n'
      ))
    )),
    composite_field: ($) => prec.left(seq(
      field("name",$.ident),
      ":",
      field("value",$.call_type),
      repeat($.eol),
    )),
    call_type: $ => prec(5,seq(
      $.ident,
      optional("[]"),
    )),
    composite_operator: $ => choice(
      "|",
      "&",
    ),
    composite_expr: ($) => seq(
      $.composite_operator,
      $.composite_field,
    ),
    composite_type: ($) => seq(
      prec.left($.composite_field),
      prec.left(repeat($.composite_expr)),
    ),
    alias_type:($) => seq(
      $.call_type,
      $.eol,
    )
  },
});
